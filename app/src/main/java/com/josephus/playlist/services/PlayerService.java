package com.josephus.playlist.services;

import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;
import java.io.IOException;

/**
 * Created by JosephusVillarey on 13/08/2015.
 */
public class PlayerService extends Service {

  private static final String TAG = PlayerService.class.getSimpleName();

  public static final String EXTRA_PLAYLIST = "EXTRA_PLAYLIST";
  public static final String EXTRA_FROM_POSITION = "EXTRA_POSITION";
  private boolean isPlaying = false;
  private int playingIndex = 0;
  private String uri = null;
  private MediaPlayer player = null;

  @Override public int onStartCommand(Intent intent, int flags, int startId) {
    String[] playlist = intent.getStringArrayExtra(EXTRA_PLAYLIST);
    play(playlist, intent.getIntExtra(EXTRA_FROM_POSITION, 0));
    return START_NOT_STICKY;
  }

  @Override public void onDestroy() {
    stop();
  }

  private void stop() {
    if (isPlaying) {
      isPlaying = false;
      stopForeground(true);
    }
  }

  private void play(final String[] playlist, final int index) {
    if (!isPlaying) {
      playingIndex = index;
      isPlaying = true;
      uri = playlist[playingIndex];
      player = new MediaPlayer();
      final Uri myUri = Uri.parse(uri);
      player.setAudioStreamType(AudioManager.STREAM_MUSIC);
      try {
        player.setDataSource(getApplicationContext(), myUri);
        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
          @Override public void onCompletion(MediaPlayer mp) {
            playingIndex++;
            uri = playlist[playingIndex];
            try {
              player.reset();
              player.setDataSource(getApplicationContext(), Uri.parse(uri));
              player.prepare();
              player.start();
            } catch (IOException e) {
              e.printStackTrace();
              isPlaying = false;
            }
          }
        });
        player.prepare();
        player.start();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  @Nullable @Override public IBinder onBind(Intent intent) {
    return null;
  }
}
