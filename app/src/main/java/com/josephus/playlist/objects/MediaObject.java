package com.josephus.playlist.objects;

import android.media.MediaMetadataRetriever;

/**
 * Created by JosephusVillarey on 13/08/2015.
 */
public class MediaObject {

  public String title, artist, duration, path;

  public MediaObject(String path) {
    this.path = path;
    MediaMetadataRetriever retriever = new MediaMetadataRetriever();
    retriever.setDataSource(this.path);
    this.title = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
    this.artist = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
    this.duration = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
    retriever.release();
  }
}
