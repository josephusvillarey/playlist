package com.josephus.playlist.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.josephus.playlist.R;
import com.josephus.playlist.objects.MediaObject;
import java.util.List;

/**
 * Created by JosephusVillarey on 13/08/2015.
 */
public class PlayableFileAdapter extends BaseAdapter {

  private static final String TAG = PlayableFileAdapter.class.getSimpleName();

  private Context context;
  private List<MediaObject> files;

  public PlayableFileAdapter(Context context, List<MediaObject> files) {
    this.context = context;
    this.files = files;
  }

  @Override public int getCount() {
    return files.size();
  }

  @Override public MediaObject getItem(int position) {
    return files.get(position);
  }

  @Override public long getItemId(int position) {
    return position;
  }

  @Override public View getView(int position, View convertView, ViewGroup parent) {

    ViewHolder holder = null;
    if (convertView == null) {
      holder = new ViewHolder();
      convertView = LayoutInflater.from(context).inflate(R.layout.playable_item, parent, false);

      holder.title = (TextView) convertView.findViewById(R.id.title);
      holder.artist = (TextView) convertView.findViewById(R.id.artist);
      holder.duration = (TextView) convertView.findViewById(R.id.duration);

      convertView.setTag(holder);
    } else {
      holder = (ViewHolder) convertView.getTag();
    }

    // get retriever
    //MediaMetadataRetriever retriever = new MediaMetadataRetriever();
    //retriever.setDataSource(getItem(position).getPath());
    String title = getItem(position).title, artist = getItem(position).artist, duration =
        getItem(position).duration;

    holder.title.setText(title);
    holder.artist.setText(artist);
    holder.duration.setText(duration);

    // release retriever
    return convertView;
  }

  static class ViewHolder {
    TextView title, artist, duration;
  }
}
