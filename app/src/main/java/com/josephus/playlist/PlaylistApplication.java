package com.josephus.playlist;

import android.app.Application;
import android.support.annotation.Nullable;

/**
 * Created by JosephusVillarey on 13/08/2015.
 */
public class PlaylistApplication extends Application {
  private static final String TAG = PlaylistApplication.class.getSimpleName();
  private static final String KEY_PLAYLIST_PATH = "playlist_path";

  private static PlaylistApplication mInstance;

  @Override public void onCreate() {
    super.onCreate();
    mInstance = this;
  }

  public static synchronized PlaylistApplication getInstance() {
    return mInstance;
  }

  @Nullable public String getStoredPlaylistPath() {
    return getSharedPreferences(TAG, MODE_PRIVATE).getString(KEY_PLAYLIST_PATH, null);
  }

  public void setStoredPlaylistPath(String path) {
    getSharedPreferences(TAG, MODE_PRIVATE).edit().putString(KEY_PLAYLIST_PATH, path).apply();
  }
}
