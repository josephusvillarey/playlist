package com.josephus.playlist.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.josephus.playlist.MainActivity;

/**
 * Created by JosephusVillarey on 13/08/2015.
 */
public class BootReceiver extends BroadcastReceiver {
  @Override public void onReceive(Context context, Intent intent) {
    if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
      Intent myStarterIntent = new Intent(context, MainActivity.class);
      myStarterIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      context.startActivity(myStarterIntent);
    }
  }
}
