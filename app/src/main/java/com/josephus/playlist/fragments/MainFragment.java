package com.josephus.playlist.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.josephus.playlist.PlaylistApplication;
import com.josephus.playlist.R;
import com.josephus.playlist.adapters.PlayableFileAdapter;
import com.josephus.playlist.objects.MediaObject;
import com.josephus.playlist.services.PlayerService;
import com.nononsenseapps.filepicker.FilePickerActivity;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainFragment extends Fragment {

  @Bind(R.id.list) ListView list;
  @Bind(R.id.path) public EditText path;

  private static final String TAG = MainFragment.class.getSimpleName();

  public static final int FILE_PICKER_REQUEST_CODE = 2;

  public MainFragment() {
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_main, container, false);
    ButterKnife.bind(this, view);
    return view;
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    try {
      String storedPlaylistPath = PlaylistApplication.getInstance().getStoredPlaylistPath();
      if (storedPlaylistPath != null) {
        handleReturnedPath(Uri.parse(storedPlaylistPath));
      }
    } catch (NullPointerException e) {
      e.printStackTrace();
    }
  }

  @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
    switch (requestCode) {
      case MainFragment.FILE_PICKER_REQUEST_CODE:
        if (resultCode == Activity.RESULT_OK) {
          if (data.getBooleanExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false)) {
            // For JellyBean and above
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
              ClipData clip = data.getClipData();
              if (clip != null) {
                for (int i = 0; i < clip.getItemCount(); i++) {
                  Uri uri = clip.getItemAt(i).getUri();
                  handleReturnedPath(uri);
                }
              }
              // For Ice Cream Sandwich
            } else {
              ArrayList<String> paths =
                  data.getStringArrayListExtra(FilePickerActivity.EXTRA_PATHS);
              if (paths != null) {
                for (String path : paths) {
                  Uri uri = Uri.parse(path);
                  handleReturnedPath(uri);
                }
              }
            }
          } else {
            Uri uri = data.getData();
            handleReturnedPath(uri);
          }
        }
        break;
      default:
        super.onActivityResult(requestCode, resultCode, data);
        break;
    }
  }

  public void handleReturnedPath(Uri uriPath) {
    path.setText(uriPath.toString());
    buildPlaylist(new File(uriPath.getPath()));
  }

  @OnClick(R.id.pick) public void pickClicked(View view) {
    Intent i = new Intent(getActivity(), FilePickerActivity.class);
    i.putExtra(FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, false);
    i.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false);
    i.putExtra(FilePickerActivity.EXTRA_MODE, FilePickerActivity.MODE_DIR);
    startActivityForResult(i, FILE_PICKER_REQUEST_CODE);
  }

  private void buildPlaylist(File directory) {
    new CreatePlaylistTask().execute(directory);
  }

  class CreatePlaylistTask extends AsyncTask<File, Void, List<MediaObject>> {

    private AlertDialog d = null;

    @Override protected void onPreExecute() {
      super.onPreExecute();
      d = new AlertDialog.Builder(getActivity()).setTitle("Creating Playlist")
          .setMessage("Please wait...")
          .show();
    }

    @Override protected List<MediaObject> doInBackground(File... params) {
      final String[] acceptedExtensions =
          getResources().getStringArray(R.array.accepted_file_extensions);
      File[] filesInDirectory = params[0].listFiles(new FilenameFilter() {
        @Override public boolean accept(File dir, String filename) {
          for (String s : acceptedExtensions) {
            if (filename.endsWith(s)) {
              return true;
            }
          }
          return false;
        }
      });
      List<MediaObject> mp3Files = new ArrayList<MediaObject>();
      for (File f : filesInDirectory) {
        mp3Files.add(new MediaObject(f.getAbsolutePath()));
      }
      PlaylistApplication.getInstance().setStoredPlaylistPath(params[0].getAbsolutePath());
      return mp3Files;
    }

    @Override protected void onPostExecute(List<MediaObject> mediaObjects) {
      super.onPostExecute(mediaObjects);
      d.dismiss();
      list.setAdapter(new PlayableFileAdapter(getActivity(), mediaObjects));
      // convert mediaObjects to string array of paths
      String[] paths = new String[mediaObjects.size()];
      for (int index = 0; index < mediaObjects.size(); index++) {
        paths[index] = mediaObjects.get(index).path;
      }

      // start playing
      Intent i = new Intent(getActivity(), PlayerService.class);
      i.putExtra(PlayerService.EXTRA_PLAYLIST, paths);
      i.putExtra(PlayerService.EXTRA_FROM_POSITION, 0);
      getActivity().startService(i);
    }
  }
}
